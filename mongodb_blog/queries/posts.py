import os
import pymongo
from models import PostIn, PostOut, Posts
from pymongo.collection import ReturnDocument
from bson.objectid import ObjectId
from typing import List

dbhost = os.environ['MONGOHOST']
dbname = os.environ['MONGODATABASE']
dbuser = os.environ['MONGOUSER']
dbpass = os.environ['MONGOPASSWORD']
mongo_str = f"mongodb://{dbuser}:{dbpass}@{dbhost}"

client = pymongo.MongoClient(mongo_str)


class BlogQueries:
    def create(self, post: PostIn) -> PostOut:
        db = client[dbname]
        props = post.dict()
        result = db.posts.insert_one(props)
        props["id"] = str(props["_id"])
        return PostOut(**props)

    def get_all(self) -> List[PostOut]:
        db = client[dbname]
        result = db.posts.find()
        print(result)
        posts_list = list(result)
        for post in posts_list:
            post["id"] = str(post["_id"])
        return [PostOut(**post) for post in posts_list]

    def get_one(self, id) -> PostOut:
        db = client[dbname]
        result = db.posts.find_one({ "_id": ObjectId(id) })
        if result:
            result["id"] = str(result["_id"])
            return PostOut(**result)

    def update(self, id, body) -> PostOut:
        db = client[dbname]
        post = db.posts.find_one({ "_id": ObjectId(id) })
        updated_post = db.posts.find_one_and_update(post, {"$set": body}, return_document=ReturnDocument.AFTER)
        updated_post["id"] = str(updated_post["_id"])
        return PostOut(**updated_post)

    def delete(self, id):
        db = client[dbname]
        db.posts.delete_one({ "_id": ObjectId(id) })
