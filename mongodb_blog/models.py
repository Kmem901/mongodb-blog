from bson.objectid import ObjectId
from pydantic import BaseModel
from datetime import datetime


class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError("Invalid objectid")
        return ObjectId(v)


class PostIn(BaseModel):
    title: str
    author: str
    created: datetime
    body: str

class PostOut(BaseModel):
    id: str
    title: str
    author: str
    created: datetime
    body: str

class Posts(BaseModel):
    posts: list[PostOut]

class Post(PostIn):
    id: PyObjectId
