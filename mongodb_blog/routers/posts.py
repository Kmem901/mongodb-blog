from fastapi import APIRouter, Depends, Response
from models import PostIn, PostOut, Posts
from queries.posts import BlogQueries

router = APIRouter()

@router.post("/posts", response_model=PostOut)
def create(post: PostIn, repo: BlogQueries = Depends()):
    post = repo.create(post)
    return post

@router.get("/posts", response_model=Posts)
def get_all(repo: BlogQueries = Depends()):
    return Posts(posts=repo.get_all())

@router.get("/posts/{post_id}", response_model=PostOut)
def get_one(post_id: str, response: Response, repo: BlogQueries = Depends()):
    record = repo.get_one(post_id)
    if record is None:
        response.status_code = 404
    else:
        return record

@router.put("/posts/{post_id}", response_model=PostOut)
def update(post_id: str, body: dict, repo: BlogQueries = Depends()):
    post = repo.update(post_id, body)
    return post

@router.delete("/posts/{post_id}", response_model=bool)
def delete(post_id: str, repo: BlogQueries = Depends()):
    repo.delete(id=post_id)
    return True
